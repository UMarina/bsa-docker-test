FROM php:7.2-fpm
COPY index.php /var/www
WORKDIR /var/www
CMD ["php" ,"-S" ,"0.0.0.0:8000"]
EXPOSE 8000


